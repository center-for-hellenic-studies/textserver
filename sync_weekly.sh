# set flag for weekly synced repositories
export REPO_FILE=repositories_weekly.json

#rm -rf ./tmp/First1KGreek

# update weekly synced repositories
cd ./tmp/First1KGreek
git pull
cd ../..

# perform sync and log
nvm use 8.16.0
nohup npm run db:ingest > weekly.log &