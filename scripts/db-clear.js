import dotenv from 'dotenv';

import dotenvSetup from '../src/dotenv.js';
import {
	Author,
	Collection,
	Exemplar,
	Language,
	TextGroup,
	TextNode,
	Version,
	Work,
} from '../src/models/index.js';
import db, { dbSetup } from '../src/db.js';
import logger from '../src/logger.js';

// setup environment variables and db connection
dotenvSetup();
dbSetup();

db.authenticate().then(async () => {
	// sync database
	const sync = await db.sync();

	// destory all
	logger.info('Dropping all tables in database');
	await Author.destroy({
		where: {},
	});
	await Collection.destroy({
		where: {},
	});
	await Exemplar.destroy({
		where: {},
	});
	await Language.destroy({
		where: {},
	});
	await TextGroup.destroy({
		where: {},
	});
	await TextNode.destroy({
		where: {},
	});
	await Version.destroy({
		where: {},
	});
	await Work.destroy({
		where: {},
	});

	// close db
	return db.close();
});
