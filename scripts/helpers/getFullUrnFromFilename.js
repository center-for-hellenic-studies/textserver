
const generateFullUrnForWork = (work) => {
	const filenameArr = work.filename.split('.')[0].split('__');
	const lastIndex = filenameArr.length - 1;
	const languagePart = filenameArr[lastIndex];
	const textNamePart = filenameArr[lastIndex - 1];
	const fullUrn = `${work.urn}.${textNamePart}-${languagePart}`;
	console.log(`[GenerateFullURN] urn: ${work.urn} ... filename: ${work.filename} ... full_urn: ${fullUrn}`);
	return fullUrn;
};


export default generateFullUrnForWork;
