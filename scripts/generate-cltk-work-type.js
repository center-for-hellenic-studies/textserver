import dotenvSetup from '../src/dotenv.js';
import * as models from '../src/models/index.js';
import db, { dbSetup } from '../src/db.js';
import logger from '../src/logger.js';

import getCLTKWorkType from './helpers/getCLTKWorkType';

// setup environment variables and db connection
dotenvSetup();
dbSetup();

const generateCLTKWorkType = async () => {
	try {
		const args = {
			where: {
				filename: {
					$like: '%cltk%',
				},
			},
			// limit: 100,
		};
		const allCLTKWorks = await models.Work.findAll(args);

		for (let i = 0; i < allCLTKWorks.length; i += 1) {
			const work = allCLTKWorks[i];
			if (!work.work_type) {
				const worType = await getCLTKWorkType(work); // eslint-disable-line
				work.work_type = worType;
				console.log(`[INFO] ${work.filename} ... ${work.work_type}`);
				await work.save(); // eslint-disable-line
			}
		}

		return 'generateCLTKWorkType completed.';
	} catch (e) {
		logger.error(e);
		return 'Error. Aborting.';
	}
};

db.authenticate().then(async () => {
	// sync database
	await db.sync();

	// run
	logger.info('Beginning generateCLTKWorkType');
	const result = await generateCLTKWorkType();
	logger.info(result);

	// close db
	db.close();
});
