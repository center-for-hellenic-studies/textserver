import dotenvSetup from '../src/dotenv.js';
import * as models from '../src/models/index.js';
import db, { dbSetup } from '../src/db.js';
import logger from '../src/logger.js';

import generateFullUrnForWork from './helpers/getFullUrnFromFilename';

// setup environment variables and db connection
dotenvSetup();
dbSetup();

const generateFullUrnCLTK = async () => {
	try {
		const args = {
			where: {
				filename: {
					$like: '%cltk%',
				},
			},
			// limit: 3,
		};
		const allCLTKWorks = await models.Work.findAll(args);

		for (let i = 0; i < allCLTKWorks.length; i += 1) {
			const work = allCLTKWorks[i];
			if (!work.full_urn) {
				const fullUrn = generateFullUrnForWork(work).substring(0, 255);
				work.full_urn = fullUrn;
				await work.save(); // eslint-disable-line
			}
		}

		return 'generateFullUrnCLTK completed.';
	} catch (e) {
		logger.error(e);
		return 'Error with migration. Aborting.';
	}
};

db.authenticate().then(async () => {
	// sync database
	await db.sync();

	// run
	logger.info('Beginning generate full_urn for cltk texts');
	const result = await generateFullUrnCLTK();
	logger.info(result);

	// close db
	db.close();
});
