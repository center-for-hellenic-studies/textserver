import dotenv from 'dotenv';

import dotenvSetup from '../src/dotenv.js';
import * as models from '../src/models/index.js';
import db, { dbSetup } from '../src/db.js';
import { ingestCollections } from '../src/modules/cts/index.js';
import logger from '../src/logger.js';

// setup environment variables and db connection
dotenvSetup();
dbSetup();

const ingest = async () => {
	let ingestResult;

	try {
		return await ingestCollections();
	} catch (e) {
		logger.error(e);
		return 'Error with ingest. Aborting.';
	}
};

db.authenticate().then(async () => {
	// sync database
	const sync = await db.sync();

	// run ingest
	logger.info('Beginning ingest of designated repositories');
	const ingestResult = await ingest();
	logger.info(ingestResult);
	logger.info('[INFO] db:ingest completed.');

	// close db
	db.close();
});
