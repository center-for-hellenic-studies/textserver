import path from 'path';
import fs from 'fs';

import { ingestCollections } from './modules/cts/index.js';

/**
 * saves incoming uploaded text in a specific folder for further ingestion
 * @param {*} filename
 * @param {*} content
 */
const saveText = async (filename, content) => {
	console.log('Received file - ', filename, content.length);

	const textUploadedPath = path.join(
		'text_uploaded/data/uploaded/new',
		filename
	);

	try {
		const data = fs.writeFileSync(textUploadedPath, content);
		console.log('Saved file - ', textUploadedPath);
	} catch (err) {
		console.error(err);
	}
};

/**
 * ingest text files in text_uploaded folder
 * @param {*} filename
 * @param {*} content
 */
const ingestText = async () => {
	const textUploaded = {
		title: 'Uncategorized Newly Uploaded Texts',
		repoRemote: '',
		repoLocal: 'text_uploaded',
	};
	const textUploadedOnly = [textUploaded];

	try {
		return await ingestCollections(textUploadedOnly);
	} catch (e) {
		console.log(e);
		return 'Error with ingest. Aborting.';
	}
};

/**
 * save file and start ingestion
 * @param {*} filename
 * @param {*} content
 */
const processTexts = async (filename, content) => {
	await saveText(filename, content);
	await ingestText();
};

/**
 * start parsing received file content and returns ok
 * @param {*} req
 * @param {*} res
 */
const receiveText = async (req, res) => {
	await processTexts(req.body.filename, req.body.content);
	res.send({ ok: true });
};

export default receiveText;
