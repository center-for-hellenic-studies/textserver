import parsePassage from './parsePassage.js';

const serializeLwCTSUrn = (value) => {
	if (!value) {
		return '';
	}
	if (typeof value === 'string') {
		return value;
	}

	let result = '';

	if ('textGroup' in value && value.textGroup && value.textGroup.length) {
		result = `${result}${value.textGroup}`;
	} else {
		return result;
	}

	if ('work' in value && value.work && value.work.length) {
		result = `${result}.${value.work}`;
	} else {
		return result;
	}

	/** version, exemplar, and translation are optional but must be in order */
	if ('version' in value && value.version && value.version.length) {
		result = `${result}.${value.version}`;
		if ('exemplar' in value && value.exemplar && value.exemplar.length) {
			result = `${result}.${value.exemplar}`;
			if (
				'translation' in value &&
				value.translation &&
				value.translation.length
			) {
				result = `${result}.${value.translation}`;
			}
		}
	}

	if ('passage' in value && value.passage && value.passage.length) {
		result = `${result}:${parsePassage(value.passage)}`;
	}

	return result;
};

export default serializeLwCTSUrn;
