import fs from 'fs';
import crypto from 'crypto';
import _s from 'underscore.string';

import { getCltkTextgroupUrn } from '../../lib/getCltkUrns.js';
import Language from '../../../../models/language.js';
import TextGroup from '../../../../models/textGroup.js';
import Work from '../../../../models/work.js';
import TextNode from './TextNode.js';
import Version from './Version.js';
import Translation from './Translation.js';
import logger from '../../../../logger.js';

/** Class representing a work in a textgroup */
class _Work {
	/**
	 * Create a new work
	 */
	constructor({ text, urn, filename }) {
		this.text = text;
		this.urn = urn;
		this.original_title = text.originalTitle || text.englishTitle;
		this.english_title = text.englishTitle;
		this.filename = filename;
		this.filemd5hash = crypto
			.createHash('md5')
			.update(fs.readFileSync(filename, 'utf8'))
			.digest('hex');

		// metadata about work
		this.language = text.language;
		this.structure = null;
		this.form = null;
		// edition information about work
		this.version = null;
		this.exemplar = null;
		this.translation = null;
		// textnodes for work
		this.textNodes = [];

		// generate full urn from filename and urn
		this.full_urn = this.generateFullUrn();
	}

	generateFullUrn() {
		const filenameArr = this.filename.split('.')[0].split('__');
		const lastIndex = filenameArr.length - 1;
		const languagePart = filenameArr[lastIndex];
		const textNamePart = filenameArr[lastIndex - 1];
		const fullUrn = `${this.urn}.${textNamePart}-${languagePart}`;
		return fullUrn;
	}

	/**
	 * Generate the inventory of the textNodes in the work
	 */
	async generateInventory(collection) {
		logger.info(
			` -- --  -- generating inventory for work ${this.english_title}`
		);

		const jsonToTextNodes = (node, location = []) => {
			for (const key in node) {
				// eslint-disable-line
				const _location = location.slice();
				_location.push(parseInt(key, 10) + 1);
				if (typeof node[key] === 'string') {
					this.textNodes.push(
						new TextNode({
							location: _location,
							text: node[key],
							filename: this.filename,
						})
					);
				} else {
					jsonToTextNodes(node[key], _location);
				}
			}
		};

		// convert json document body to textnodes
		jsonToTextNodes(this.text.text);

		// set edition version if edition in json
		if ('edition' in this.text) {
			let editionUrn = `${this.urn}`;

			// set edition urn from text metadata
			if (this.text.source === 'The Center for Hellenic Studies') {
				if (this.text.language === 'english') {
					editionUrn = `${editionUrn}.chs-translation`;
					this.work_type = 'translation';
				} else {
					editionUrn = `${editionUrn}.chs-${_s.camelize(
						_s.slugify(this.text.edition)
					)}`;
					this.work_type = 'edition';
				}
			}

			this.version = new Version({
				title: this.text.edition,
				urn: editionUrn,
			});
		}

		// remove existing textnodes
		await Work.destroy({
			// eslint-disable-line
			where: {
				filename: this.filename,
				// filemd5hash: this.sourceFileMD5, // debug
			},
		});

		// when a work has no textnode, remove its existing instances and skip saving
		if (this.textNodes.length === 0) {
			console.info(
				'[INFO] Ignore work with no textnodes: ',
				this.filename
			);
			return;
		}

		return await this.save(collection);
	}

	/**
	 * Save all work data and textnodes in the work
	 */
	async save(collection) {
		let textGroup = await TextGroup.findOne({
			where: {
				title: this.text.author,
				collectionId: collection.id, // some textGroup share the same author "Not Available"
			},
		});

		if (!textGroup) {
			textGroup = await TextGroup.create({
				title: this.language,
				urn: getCltkTextgroupUrn(collection.urn, this.text.author),
			});
		}

		const englishTitle = this.english_title;
		const originalTitle = this.original_title;
		if (!englishTitle || !originalTitle) {
			logger.error(`Error ingesting Work ${this.filename}`);
			return null;
		}

		const urn = this.urn || '';

		// de-dup work
		const existingWorkItems = await Work.findAll({
			// eslint-disable-line
			// attributes: ['filename', 'filemd5hash', 'id'],
			where: {
				filename: this.filename,
				// filemd5hash: this.sourceFileMD5, // debug
			},
			// raw: true,
		});
		let existingWork = null;
		if (existingWorkItems.length > 0) {
			existingWork = existingWorkItems[0];
		}
		const purgeWorkIDs = existingWorkItems.slice(1).map((work) => work.id);
		if (purgeWorkIDs.length > 0) {
			console.info('Removing duplicated works: ', purgeWorkIDs);
			await Work.destroy({
				// eslint-disable-line
				where: { id: { $in: purgeWorkIDs } },
			});
		}

		let work;
		if (existingWork) {
			work = await existingWork.updateAttributes({
				filemd5hash: this.filemd5hash,
				filename: this.filename,
				original_title: originalTitle.slice(0, 255),
				english_title: englishTitle.slice(0, 255),
				structure: this.structure,
				form: this.form,
				urn: urn.slice(0, 255),
				full_urn: this.full_urn,
				work_type: this.work_type,
			});
		} else {
			work = await Work.create({
				filemd5hash: this.filemd5hash,
				filename: this.filename,
				original_title: originalTitle.slice(0, 255),
				english_title: englishTitle.slice(0, 255),
				structure: this.structure,
				form: this.form,
				urn: urn.slice(0, 255),
				full_urn: this.full_urn,
				work_type: this.work_type,
			});
		}

		await work.setTextgroup(textGroup);
		await textGroup.addWork(work);

		await this._createLanguage(work);

		if (this.version) {
			await this.version.save(work);
		}

		// ingest all textnodes
		for (let i = 0; i < this.textNodes.length; i += 1) {
			await this.textNodes[i].save(i); // eslint-disable-line
		}
	}

	async _createLanguage(work) {
		let language;
		language = await Language.findOne({
			where: {
				title: _s.humanize(this.language),
			},
		});

		const _language = _s.humanize(this.language).trim();
		if (!language && _language.length) {
			language = await Language.create({
				title: _language,
			});
		}

		await work.setLanguage(language);
		await language.addWork(work);

		if (
			~['german', 'french', 'english', 'italian'].indexOf(this.language)
		) {
			let translationUrn = this.urn;

			// set translation urn from text metadata
			if (this.text.source === 'The Center for Hellenic Studies') {
				translationUrn = `${translationUrn}.chs-translation-${_s.slugify(
					this.language
				)}`;
			}

			this.translation = new Translation({
				title: `${this.english_title} Translation`,
				urn: translationUrn,
			});

			await this.translation.save(work);
		}
	}
}

export default _Work;
