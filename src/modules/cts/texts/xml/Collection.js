import fs from 'fs';
import path from 'path';
import slugify from 'slugify';
import { DOMParser } from '@xmldom/xmldom';

import Collection from '../../../../models/collection.js';
import TextGroup from './TextGroup.js';
import getCollectionUrnByTitle from '../../lib/getCollectionUrnByTitle.js';
import logger from '../../../../logger.js';

class _Collection {
	/**
	 * Create a collection
	 */
	constructor({ title, repoRemote, repoLocal, collectionDataType }) {
		this.title = title;
		this.urn = getCollectionUrnByTitle(title);
		this.repoRemote = repoRemote;
		this.repoLocal = repoLocal;
		this.collectionDataType = collectionDataType;
		this.textGroups = [];
	}

	/**
	 * Get the inventory of this collection's textGroups
	 */
	async generateInventory() {
		logger.info(` -- generating inventory for collection ${this.title}`);
		// walk contents of textgroup dir
		const textGroupContents = fs.readdirSync(
			path.join(this.repoLocal, '/data/')
		);
		textGroupContents.forEach((textGroupContent, i) => {
			// if the content object is a directory
			if (
				fs
					.lstatSync(
						path.join(this.repoLocal, '/data/', textGroupContent)
					)
					.isDirectory()
			) {
				if (
					!fs.existsSync(
						path.join(
							this.repoLocal,
							'/data/',
							textGroupContent,
							'__cts__.xml'
						)
					)
				) {
					return false;
				}

				// check textgroup for __cts__.xml metadata file
				const _textGroupMetadataFile = fs.readFileSync(
					path.join(
						this.repoLocal,
						'/data/',
						textGroupContent,
						'/__cts__.xml'
					),
					'utf8'
				);

				// handle case of no __cts__.xml file
				if (!_textGroupMetadataFile) {
					logger.info(
						`No metadata file for ${path.join(
							this.repoLocal,
							'/data/',
							textGroupContent
						)}, skipping.`
					);
					return false;
				}

				// parse xml
				const _textGroupXML = new DOMParser().parseFromString(
					_textGroupMetadataFile
				);

				// create a new textGroup
				const textGroup = new TextGroup({
					textGroupDir: path.join(
						this.repoLocal,
						'/data/',
						textGroupContent
					),
					_textGroupXML,
				});

				// add to collection textgroups array
				this.textGroups.push(textGroup);
			}
		});

		// skip creating collection for text_uploaded
		if (~this.repoLocal.indexOf('text_uploaded')) {
			return await this.saveTextUploaded();
		}

		return await this.save();
	}

	/**
	 * Save all textgroups in collection inventory (will save all hierarchical
	 * related data in the collection>>textgroup>>work>>textNode tree)
	 */
	async save() {
		const title = this.title;

		if (!title) {
			logger.error(`Error ingesting Collection ${this.repoLocal}`);
			return null;
		}

		// collection de-dup
		let collection = await Collection.findOne({
			where: { repository: this.repoRemote },
		});
		if (collection) {
			collection = await collection.update({
				title: title.slice(0, 250),
				repository: this.repoRemote,
				urn: this.urn,
			});
		} else {
			collection = await Collection.create({
				title: title.slice(0, 250),
				urn: this.urn,
				repository: this.repoRemote,
			});
		}

		for (let i = 0; i < this.textGroups.length; i += 1) {
			await this.textGroups[i].generateInventory(collection); // eslint-disable-line
		}
	}

	/**
	 * make sure text uploaded Collection exists
	 * make sure textgroup newly uploaded exists
	 * get all xml texts (works) and check file hash to find new ones
	 * add new ones to textgroup-works
	 * save textgroup-works
	 */
	async saveTextUploaded() {
		// make sure text uploaded Collection exists
		let collection = await Collection.findOne({
			where: { urn: 'urn:cts:uploaded' },
		});
		if (collection) {
			console.log('Found Collection urn:cts:uploaded');
		} else {
			collection = await Collection.create({
				title: this.title,
				urn: 'urn:cts:uploaded',
			});
		}

		for (let i = 0; i < this.textGroups.length; i += 1) {
			await this.textGroups[i].generateInventory(collection); // eslint-disable-line
		}
	}
}

export default _Collection;
