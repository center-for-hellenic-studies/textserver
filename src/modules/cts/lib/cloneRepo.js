import fs from 'fs';
import git from 'simple-git';
import logger from '../../../logger.js';

/**
 * Clone a repository
 * TODO: determine optimal method of error handling with async/await
 */
const cloneRepo = async (repository, repoLocal) => {
	if (!fs.existsSync(repoLocal)) {
		try {
			return await git().clone(repository, repoLocal);
		} catch (e) {
			logger.error(`Unable to clone repo. Reason: ${e}`);
			return false;
		}
	}
	logger.info(` -- repo already cloned at ${repoLocal}`);
	return false;
};

export default cloneRepo;
