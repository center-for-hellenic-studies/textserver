/**
 * define config, constants, for CTS XMLs
 */

export const WORKTYPE_EDITION = 'edition';
export const WORKTYPE_TRANSLATION = 'translation';
