import fs from 'fs';
import path from 'path';
import logger from '../../../logger.js';

/**
 * Check if is xml or json data in the collection repo
 */
const checkXmlOrJSON = (collectionDir) => {
	// the check for uploaded text only
	if (~collectionDir.indexOf('text_uploaded')) {
		return 'xml';
	}

	// the normal check
	let collectionFiles = null;

	try {
		collectionFiles = fs.readdirSync(collectionDir);
	} catch (e) {
		logger.error(`Could not read collection directory. Reason: ${e}`);
		return false;
	}

	if (
		~collectionFiles.indexOf('data') &&
		!~collectionFiles.indexOf('cltk_json')
	) {
		return 'xml';
	}

	return 'json';
};

export default checkXmlOrJSON;
