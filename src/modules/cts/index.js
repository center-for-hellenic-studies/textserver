import * as path from 'path';
import slugify from 'slugify';
import * as fs from 'fs';

import cloneRepo from './lib/cloneRepo.js';
import ingestCollection from './lib/ingestCollection.js';
import logger from '../../logger.js';

const REPO_LOCATION = process.env.REPO_LOCATION || './tmp';

const cloneRepos = async () => {
	const clonedRepos = [];
	const clonePromises = [];

	const repositoriesFileName = process.env.REPO_FILE || 'repositories.json';
	logger.info(`[INFO] Using repositories file: ${repositoriesFileName}`);

	const repositoriesFile = fs.readFileSync(
		path.join('.', repositoriesFileName)
	);
	const repositoriesJSON = JSON.parse(repositoriesFile);
	repositoriesJSON.repositories.forEach((repository) => {
		// set local repo path
		let repositoryLocal = repository.url.substring(
			repository.url.lastIndexOf('/')
		);
		repositoryLocal = path.join(
			REPO_LOCATION,
			repositoryLocal.replace(path.extname(repositoryLocal), '')
		);
		logger.info(` -- cloning ${repository.title} into ${repositoryLocal}`);

		// keep copy of all cloned repos' remote/local data
		clonedRepos.push({
			title: repository.title,
			repoRemote: repository.url,
			repoLocal: repositoryLocal,
		});

		// clone repo
		clonePromises.push(cloneRepo(repository.url, repositoryLocal));
	});

	// add uncategorized books
	clonedRepos.push({
		title: 'Uncategorized',
		repoRemote: '',
		repoLocal: path.join(REPO_LOCATION, 'uncategorized'),
	});

	await Promise.all(clonePromises);
	return clonedRepos;
};

/**
 * @param repos for manually specify repositories
 */
const ingestCollections = async (repos = []) => {
	// setup tmp dir
	const dir = REPO_LOCATION;
	if (!fs.existsSync(dir)) {
		fs.mkdirSync(dir);
	}

	let _clonedRepos;
	if (repos.length > 0) {
		// use specified repos
		logger.info('Using specified repositories ...', repos);
		_clonedRepos = repos;
	} else {
		// clone repos
		logger.info('Cloning repositories');
		_clonedRepos = await cloneRepos();
	}

	// Ingest collections from cloned repos
	logger.info('Ingesting texts and metadata');
	for (let i = 0; i < _clonedRepos.length; i += 1) {
		logger.info(
			` -- ingesting for repo ${_clonedRepos[i].title} at ${_clonedRepos[i].repoLocal}`
		);

		// ingest data from texts in repo
		await ingestCollection(_clonedRepos[i]); // eslint-disable-line
	}
};

export { ingestCollections };
