// main
import express from 'express';
import fs from 'fs';
import http from 'http';
import path from 'path';
import { ApolloServer } from 'apollo-server-express';
import { ApolloServerPluginDrainHttpServer } from 'apollo-server-core';

// middleware
import bodyParser from 'body-parser';
import session from 'express-session';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';

// db
import * as models from './models/index.js';
import db, { dbSetup } from './db.js';

import dotenvSetup from './dotenv.js';

// Health Check
import healthCheck from './healthCheck.js';

import logger from './logger.js';

import receiveText from './receiveText.js';
import corsSetup from './cors.js';

dotenvSetup();

const IS_DEV = process.env.NODE_ENV === 'development';
const IS_PRODUCTION = process.env.NODE_ENV === 'production';

const app = express();

app.set('port', process.env.PORT || 3003);

app.use(morgan(IS_DEV ? 'dev' : 'combined'));
app.use(cookieParser());

app.use(
	bodyParser.json({
		limit: '50mb',
		type: 'application/json',
	})
);

app.use(
	bodyParser.urlencoded({
		limit: '50mb',
		extended: true,
		parameterLimit: 50000,
		type: 'application/x-www-form-urlencoded',
	})
);

// db setup
dbSetup();

// CORS setup
corsSetup(app);

// Routes
// post xml here to create and ingest text
app.post('/text', receiveText);

if (IS_DEV) {
	app.use(express.static(path.resolve(__dirname, '..', 'public')));
}

app.use('/heartbeat', healthCheck);

// App server listen
const listen = async (typeDefs, resolvers) => {
	const httpServer = http.createServer(app);
	const server = new ApolloServer({
		context: (req) => ({
			user: req.user,
			project: req.project,
		}),
		resolvers,
		typeDefs,
		plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
	});

	await server.start();

	// apollo-server-express serves at /graphql by default
	// https://www.apollographql.com/docs/apollo-server/api/apollo-server/#path
	server.applyMiddleware({ app });

	await new Promise((resolve) =>
		httpServer.listen({ port: app.get('port') }, resolve)
	);

	logger.info(
		`App is now running on http://localhost:${app.get('port')}${
			server.graphqlPath
		}`
	);

	return { app, server };
};

export { app, db, listen };
