

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.changeColumn(
      'works',
      'full_urn',
      {type: Sequelize.STRING(2048)}
    );
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.changeColumn(
      'works',
      'full_urn',
      {type: Sequelize.STRING(512)}
    );
	}
};
