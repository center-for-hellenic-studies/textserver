module.exports = {
	up: async (queryInterface, Sequelize) =>
		queryInterface.sequelize.query(
			'ALTER TABLE works ' +
				'ADD COLUMN IF NOT EXISTS work_type varchar(255), ' +
				'ADD COLUMN IF NOT EXISTS label varchar(255), ' +
				'ADD COLUMN IF NOT EXISTS description varchar(255), ' +
				'ADD COLUMN IF NOT EXISTS full_urn varchar(255);'
		),

	down: async (queryInterface, Sequelize) => [
		await queryInterface.removeColumn('works', 'work_type'),
		await queryInterface.removeColumn('works', 'label'),
		await queryInterface.removeColumn('works', 'description'),
		await queryInterface.removeColumn('works', 'full_urn'),
	],
};
