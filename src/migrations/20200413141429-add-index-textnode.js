

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.addIndex('textnodes', ['workId'], {
			name: 'idx_textnodes_workid',
		});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.removeIndex('textnodes', 'idx_textnodes_workid');
	}
};
