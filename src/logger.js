import { createLogger, format, transports } from 'winston';

const IS_PRODUCTION = process.env.NODE_ENV === 'production';

const logger = createLogger({
	level: IS_PRODUCTION ? 'info' : 'debug',
	format: format.combine(
		format.timestamp({
			format: 'YYYY-MM-DD HH:mm:ss',
		}),
		format.errors({ stack: true }),
		format.splat(),
		format.json()
	),
	defaultMeta: { service: 'textserver' },
	transports: [
		new transports.File({
			filename: 'textserver-error.log',
			level: 'error',
		}),
		new transports.File({ filename: 'textserver.log' }),
		new transports.Console({
			format: format.combine(format.colorize(), format.simple()),
		}),
	],
});

export default logger;
