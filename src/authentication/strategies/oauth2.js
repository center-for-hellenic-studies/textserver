import https from 'https';
import logger from '../../logger.js';

const validateTokenOAuth2 = (accessToken, url) =>
	new Promise((resolve, reject) => {
		https
			.get(`${url}?access_token=${accessToken}`, (res) => {
				const { statusCode } = res;

				logger.info('statusCode', statusCode);

				let error;
				if (statusCode !== 200) {
					error = new Error(
						'Request Failed.\n' + `Status Code: ${statusCode}`
					);
				}

				if (error) {
					logger.error(error.message);
					// consume response data to free up memory
					res.resume();
					return;
				}

				res.setEncoding('utf8');
				let rawData = '';
				res.on('data', (chunk) => {
					rawData += chunk;
				});
				res.on('end', () => {
					try {
						const parsedData = JSON.parse(rawData);
						resolve(parsedData);
					} catch (e) {
						logger.error(e.message);
					}
				});
			})
			.on('error', (e) => {
				logger.error(`Got error: ${e.message}`);
				reject(e);
			});
	});

export default validateTokenOAuth2;
