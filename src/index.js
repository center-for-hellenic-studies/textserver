import logger from './logger.js';

import { db, listen } from './app.js';

import resolvers from './graphql/resolvers.js';
import typeDefs from './graphql/typeDefs.cjs';

// Connect to db and then start express listen
db.authenticate()
	.then(async () => {
		await db.sync();
		logger.info(`Connected to database ${process.env.DB_NAME}`);
		listen(typeDefs, resolvers);
	})
	.catch((e) => {
		logger.error(
			`Could not authenticate to database ${process.env.DB_NAME} ... ${e}`
		);
	});
