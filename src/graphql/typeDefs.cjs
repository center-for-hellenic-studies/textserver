const fs = require('fs');
const path = require('path');
const { gql } = require('apollo-server');

const typeDefsString = fs.readFileSync(
	path.resolve(__dirname, 'typeDefs.graphql'),
	{
		encoding: 'utf-8',
	}
);

module.exports = gql(typeDefsString);
