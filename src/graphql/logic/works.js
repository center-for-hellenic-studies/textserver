import Sequelize from 'sequelize';

// logic
import PermissionsService from './PermissionsService.js';

// models
import Work from '../../models/work.js';
import Language from '../../models/language.js';
import TextGroup from '../../models/textGroup.js';
import Version from '../../models/version.js';

// lib
import serializeUrn from '../../modules/cts/lib/serializeUrn.js';

/**
 * Logic-layer service for dealing with works
 */
export default class WorkService extends PermissionsService {
	/**
	 * Create a work
	 * @param {Object} work - candidate work to create
	 * @returns {Object} newly created work
	 */
	insert(work) {
		if (this.userIsAdmin) {
			const newWork = work;
			newWork.subworks = this.rewriteSubworks(work.subworks);

			const workId = Work.insert({ ...newWork });
			return Work.findOne(workId);
		}
		return new Error('Not authorized');
	}

	/**
	 * Update a work
	 * @param {string} id - id of work
	 * @param {Object} work - work input type to be updated with
	 * @returns {Object} updated work item
	 */
	async workUpdate(id, work) {
		if (this.userIsAdmin) {
			const workInstance = await Work.findById(id);
			workInstance.updateAttributes(work);
			return workInstance.save();
		}
		return new Error('Not authorized');
	}

	/**
	 * Remove a work
	 * @param {string} _id - id of work
	 * @returns {boolean} result from mongo orm remove
	 */
	remove(_id) {
		if (this.userIsAdmin) {
			return Work.remove({ _id });
		}
		return new Error('Not authorized');
	}

	/**
	 * Get works
	 * @param {string} textsearch
	 * @param {string} urn
	 * @param {string} language
	 * @param {number} offset
	 * @param {number} limit
	 * @returns {Object[]} array of works
	 */
	async getWorks(
		textsearch,
		urn,
		fullUrn,
		language,
		offset = 0,
		limit = 100,
		textGroupId = null
	) {
		const args = {
			where: {},
			offset,
			order: [['slug', 'ASC']],
			include: [],
		};

		if (limit > 0) {
			args.limit = limit;
		}

		if (textsearch) {
			args.where.english_title = {
				[Sequelize.Op.like]: `%${textsearch}%`,
			};
		}

		if (language) {
			const languageRecord = await Language.findOne({
				where: {
					slug: language,
				},
			});
			args.include.push({
				model: Language,
				where: {
					id: languageRecord.id,
				},
			});
		}

		if (textGroupId !== null) {
			args.where.textgroupId = textGroupId;
		}

		if (urn) {
			const version = urn.version;
			delete urn.version;

			const exemplar = urn.exemplar;
			delete urn.exemplar;

			const translation = urn.translation;
			delete urn.translation;

			/**
			 * Interesting use case with CTS URNs and perhaps something to address in the future
			 * querying by verison, exemplar, and translation, but only in that order
			 */
			args.where.urn = serializeUrn(urn);
			if (version) {
				const versionRecord = await Version.findOne({
					where: {
						urn: `${serializeUrn(urn)}.${version}`,
					},
				});

				if (versionRecord) {
					args.where = {
						id: versionRecord.workId,
					};
				}

				if (exemplar) {
					const exemplarRecord = await Exemplar.findOne({
						where: {
							urn: `${serializeUrn(urn)}.${version}.${exemplar}`,
						},
					});
					if (exemplarRecord) {
						args.where = {
							id: exemplarRecord.workId,
						};
					}

					if (translation) {
						const translationRecord = await Translation.findOne({
							where: {
								urn: `${serializeUrn(
									urn
								)}.${version}.${exemplar}.${translation}`,
							},
						});
						if (translationRecord) {
							args.where = {
								id: translationRecord.workId,
							};
						}
					}
				}
			}
		}

		if (fullUrn) {
			args.where.full_urn = fullUrn;
		}

		const works = await Work.findAll(args);
		return works;
	}

	/**
	 * Get work search results
	 * @param {string} textsearch
	 * @param {string} language
	 * @param {number} offset
	 * @param {number} limit
	 * @returns {Object} work search results
	 */
	async getWorkSearch(
		textsearch,
		language,
		urns = [],
		offset = 0,
		limit = 30
	) {
		const args = {
			where: {},
			offset,
			order: [['slug', 'ASC']],
			include: [],
		};

		// when limit is set to 0, return all results
		if (limit > 0) {
			args.limit = limit;
		}

		if (textsearch) {
			args.where[Sequelize.Op.or] = {
				english_title: {
					[Sequelize.Op.like]: `%${textsearch}%`,
				},
				slug: {
					[Sequelize.Op.like]: `%${textsearch}%`,
				},
				original_title: {
					[Sequelize.Op.like]: `%${textsearch}%`,
				},
			};
		}

		if (language) {
			const languageRecord = await Language.findOne({
				where: {
					slug: language,
				},
			});
			args.include.push({
				model: Language,
				where: {
					id: languageRecord.id,
				},
			});
		}

		// filter by list of urns, is AND`ed with other criteria
		if (urns.length > 0) {
			args.where[Sequelize.Op.and] = {
				urn: { [Sequelize.Op.in]: urns },
			};
		}

		const works = await Work.findAll(args);
		const total = await Work.count(args);

		return {
			works,
			total,
		};
	}

	/**
	 * Get work
	 * @param {number} id - id of work
	 * @param {string} slug - slug of work
	 * @returns {Object} work object record
	 */
	getWork(id, slug) {
		const where = {};

		if (!id && !slug) {
			return null;
		}

		if (id) {
			where.id = id;
		}

		if (slug) {
			where.slug = slug;
		}

		return Work.findOne({ where });
	}

	/**
	 * Get work by URN
	 * @param {string} fullUrn - the work's URN. Only the first work
	 * will be found, so it's important for the URN to be complete.
	 */
	getWorkByUrn(fullUrn) {
		return Work.findOne({ where: { full_urn: fullUrn } });
	}

	/**
	 * Count works
	 * @returns {number} count of works
	 */
	count() {
		return Work.count();
	}
}
