import GraphQLJSON from 'graphql-type-json';

import AuthorService from './logic/authors.js';
import CollectionService from './logic/collections.js';
import ExemplarService from './logic/exemplars.js';
import LanguageService from './logic/languages.js';
import LightWeightCtsService from '../modules/lightWeightCts/graphql/logic/lightWeightCts.js';
import PermissionsService from './logic/PermissionsService.js';
import PerseusCtsService from '../modules/perseus/graphql/logic/perseusCts.js';
import RefsDeclService from './logic/refsDecls.js';
import TextGroupService from './logic/textGroups.js';
import TextNodeService from './logic/textNodes.js';
import TranslationService from './logic/translations.js';
import VersionService from './logic/versions.js';
import WorkService from './logic/works.js';

import CtsUrn from '../modules/cts/graphql/types/CtsUrn.js';
import LightWeightCtsResponse from '../modules/lightWeightCts/graphql/types/lightWeightCtsResponse.js';
import PerseusCtsResponse from '../modules/perseus/graphql/types/perseusCtsResponse.js';

const resolvers = {
	CtsUrn,
	JSONObject: GraphQLJSON,
	LightWeightCtsResponse,
	PerseusCtsResponse,

	Collection: {
		async textGroup(parent, { id, slug }, { token }) {
			const collectionId = parent.id;
			const textGroupService = new TextGroupService(token);
			return await textGroupService.getTextGroup(id, slug, collectionId);
		},

		async textGroups(
			parent,
			{ textsearch, urn, limit, offset },
			{ token }
		) {
			const collectionId = parent.id;
			const textGroupService = new TextGroupService(token);
			return await textGroupService.getTextGroups(
				textsearch,
				urn,
				null,
				offset,
				limit,
				collectionId
			);
		},
	},

	Mutation: {
		async refsDeclUpdate(parent, { id, refsDecl }, { token }) {
			const refsDeclService = new RefsDeclService({ token });
			return await refsDeclService.refsDeclUpdate(id, refsDecl);
		},

		async textNodeCreate(parent, { textNode }, { token }) {
			// const textNodeService = new TextNodeService({ token });
			// return await textNodeService.textNodeCreate(textNode);
		},

		async textNodeRemove(parent, { textNodeId }, { token }) {
			// const textNodeService = new TextNodeService({ token });
			// return await textNodeService.textNodeRemove(textNodeId);
		},

		async textNodeUpdate(parent, { id, textNode }, { token }) {
			const textNodeService = new TextNodeService({ token });
			return await textNodeService.textNodeUpdate(id, textNode);
		},

		async translationCreate(parent, { translation }, { token }) {
			const translationsService = new TranslationsService({ token });
			return await translationsService.translationInsert(translation);
		},

		async translationRemove(parent, { id }, { token }) {
			const translationsService = new TranslationsService({ token });
			return await translationsService.translationRemove(id);
		},

		async translationUpdate(parent, { id, translation }, { token }) {
			const translationsService = new TranslationsService({ token });
			return await translationsService.translationUpdate(id, translation);
		},

		async versionUpdate(parent, { id, version }, { token }) {
			const versionService = new VersionService({ token });
			return await versionService.versionUpdate(id, version);
		},

		async workCreate(parent, { work }, { token }) {
			const worksService = new WorksService({ token });
			return await worksService.workInsert(work);
		},

		async workRemove(parent, { workId }, { token }) {
			const worksService = new WorksService({ token });
			return await worksService.workRemove(workId);
		},

		async workUpdate(parent, { id, work }, { token }) {
			const worksService = new WorksService({ token });
			return await worksService.workUpdate(id, work);
		},
	},

	Query: {
		async author(_root, { id, slug }, { token }) {
			const authorService = new AuthorService(token);
			return await authorService.getAuthor(id, slug);
		},

		async authors(_root, { textsearch, limit, offset }, { token }) {
			const authorService = new AuthorService(token);
			return await authorService.getAuthors(textsearch, offset, limit);
		},

		async authorsCount(_root, _args, { token }) {
			const authorService = new AuthorService(token);
			return await authorService.count();
		},

		async collection(_root, { id, slug }, { token }) {
			const collectionService = new CollectionService(token);
			const collection = await collectionService.getCollection(id, slug);
			return collection;
		},

		async collections(
			_root,
			{ textsearch, urn, limit, offset },
			{ token }
		) {
			const collectionService = new CollectionService(token);
			const collections = await collectionService.getCollections(
				textsearch,
				urn,
				offset,
				limit
			);
			return collections;
		},

		async collectionsCount(_root, _args, { token }) {
			const collectionService = new CollectionService(token);
			return await collectionService.count();
		},

		async language(_root, { id, slug }, { token }) {
			const languageService = new LanguageService(token);
			return await languageService.getLanguage(id, slug);
		},

		async languages(_root, { textsearch, limit, offset }, { token }) {
			const languageService = new LanguageService(token);
			return await languageService.getLanguages(
				textsearch,
				offset,
				limit
			);
		},

		async languagesCount(_root, _args, { token }) {
			const languageService = new LanguageService(token);
			return await languageService.count();
		},

		async lightWeightCts(_, { urn }, { token }) {
			const lightWeightCtsService = new LightWeightCtsService({ token });
			const response = await lightWeightCtsService.getApiResponse({
				urn,
			});
			return response;
		},

		async perseusCts(
			_root,
			{ request = 'GetCapabilities', urn, level },
			{ token }
		) {
			const perseusCtsService = new PerseusCtsService({ token });
			const response = await perseusCtsService.getApiResponse({
				request,
				urn,
			});
			return response;
		},

		async refsDecl(_root, { id }, { token }) {
			const refsDeclService = new RefsDeclService(token);
			return await refsDeclService.getRefsDecl(id);
		},

		async textGroup(_root, { id, slug }, { token }) {
			const textGroupService = new TextGroupService(token);
			return await textGroupService.getTextGroup(id, slug);
		},

		async textGroups(
			_root,
			{ textsearch, urn, urns, limit, offset },
			{ token }
		) {
			const textGroupService = new TextGroupService(token);
			return await textGroupService.getTextGroups(
				textsearch,
				urn,
				urns,
				offset,
				limit
			);
		},

		async textGroupsCount(_root, _args, { token }) {
			const textGroupService = new TextGroupService(token);
			return await textGroupService.count();
		},

		async textNodes(
			_root,
			{
				index,
				urn,
				language,
				location,
				startsAtLocation,
				endsAtLocation,
				startsAtIndex,
				offset,
				pageSize,
				workId,
			},
			{ token }
		) {
			const textNodeService = new TextNodeService({ token });
			return textNodeService.textNodesGet(
				index,
				urn,
				language,
				location,
				startsAtLocation,
				endsAtLocation,
				startsAtIndex,
				offset,
				pageSize,
				workId
			);
		},

		async textNodeSearch(_root, args, { token }) {
			const textNodeService = new TextNodeService({ token });
			const count = await textNodeService.count(args);
			const textNodes = await textNodeService.search(args);

			let hasNextPage = false;
			let hasPreviousPage = false;
			let pages = 0;

			if (count > 0) {
				hasNextPage = await textNodeService.searchHasNextPage(
					textNodes[textNodes.length - 1],
					args
				);
				hasPreviousPage = await textNodeService.searchHasPreviousPage(
					textNodes[0],
					args
				);
				const limit = args.first || args.last || 30;
				pages = Math.ceil(count / limit);
			}

			return {
				pageInfo: { count, hasNextPage, hasPreviousPage, pages },
				textNodes,
			};
		},

		async work(_root, { id, slug }, { token }) {
			const workService = new WorkService(token);
			return await workService.getWork(id, slug);
		},

		async workByUrn(_root, { full_urn }, { token }) {
			const workService = new WorkService(token);
			return await workService.getWorkByUrn(full_urn);
		},

		async workSearch(
			_,
			{ textsearch, language, urns, limit, offset },
			{ token }
		) {
			const workService = new WorkService(token);
			const workSearchResults = await workService.getWorkSearch(
				textsearch,
				language,
				urns,
				offset,
				limit
			);
			return workSearchResults;
		},

		async works(
			_root,
			{ textsearch, urn, full_urn, language, limit, offset },
			{ token }
		) {
			const workService = new WorkService(token);
			const works = await workService.getWorks(
				textsearch,
				urn,
				full_urn,
				language,
				offset,
				limit
			);
			return works;
		},

		async worksCount(_root, _args, { token }) {
			const workService = new WorkService(token);
			return await workService.count();
		},
	},

	TextGroup: {
		collectionId(parent) {
			return parent.collectionId;
		},

		async work(parent, { id, slug }, { token }) {
			const textGroupId = parent.id;
			const workService = new WorkService(token);
			return await workService.getWork(id, slug, textGroupId);
		},

		async works(
			parent,
			{ textsearch, urn, language, edition, limit, offset },
			{ token }
		) {
			const textGroupId = parent.id;
			const workService = new WorkService(token);
			const works = await workService.getWorks(
				textsearch,
				urn,
				null, // fullUrn absent
				language,
				offset,
				limit,
				textGroupId
			);
			return works;
		},
	},

	TextNode: {
		exemplar(parent, { id, slug }, { token }) {
			const exemplarService = new ExemplarService({ token });
			return exemplarService.getExemplar(
				id,
				slug,
				parent.dataValues.workId
			);
		},

		async language(parent, __, { token }) {
			// queries could possibly be combined in the future for performance
			const workService = new WorkService({ token });
			const work = await workService.getWork(parent.dataValues.workId);

			const languageService = new LanguageService({ token });
			const language = await languageService.getLanguage(work.languageId);
			return language;
		},

		translation(parent, { id, slug }, { token }) {
			const translationService = new TranslationService({ token });
			return translationService.getTranslation(
				id,
				slug,
				parent.dataValues.workId
			);
		},

		urn(parent, _args, { token }) {
			const textNodeService = new TextNodeService({ token });
			return textNodeService.getTextNodeURN(parent);
		},

		version(parent, { id, slug }, { token }) {
			const versionService = new VersionService({ token });
			return versionService.getVersion(
				id,
				slug,
				parent.dataValues.workId
			);
		},

		words(parent, _args, { token }) {
			const textNodeService = new TextNodeService({ token });
			return textNodeService.getTextNodeWords(parent);
		},
	},

	Word: {
		urn(parent, __, { token }) {
			const textNodeService = new TextNodeService({ token });
			return textNodeService.getWordURN(parent);
		},
	},

	Work: {
		exemplar(parent, { id, slug }, { token }) {
			const exemplarService = new ExemplarService({ token });
			return exemplarService.getExemplar(id, slug, parent.dataValues.id);
		},

		language(parent, _args, { token }) {
			const languageService = new LanguageService({ token });
			return languageService.getLanguage(parent.dataValues.languageId);
		},

		refsDecls(parent, { id }, { token }) {
			const refsDeclService = new RefsDeclService({ token });
			return refsDeclService.getRefsDecls(parent.dataValues.id);
		},

		async tableOfContent(parent, { id }, { token }) {
			const textNodeService = new TextNodeService(token);
			return await textNodeService.generateTableOfContent(
				parent.dataValues.id
			);
		},

		textGroupID(parent) {
			return parent.textgroupId;
		},

		async textLocationNext(work, { index, location, offset }, { token }) {
			const textNodeService = new TextNodeService(token);
			return await textNodeService.textLocationNext(
				work.id,
				index,
				location,
				offset
			);
		},

		async textLocationPrev(work, { index, location, offset }, { token }) {
			const textNodeService = new TextNodeService(token);
			return await textNodeService.textLocationPrev(
				work.id,
				index,
				location,
				offset
			);
		},

		async textNodes(
			work,
			{
				index,
				urn,
				location,
				startsAtLocation,
				endsAtLocation,
				startsAtIndex,
				offset,
				pageSize,
			},
			{ token }
		) {
			const textNodeService = new TextNodeService({ token });
			const textNodes = await textNodeService.textNodesGet(
				index,
				urn,
				null,
				location,
				startsAtLocation,
				endsAtLocation,
				startsAtIndex,
				offset,
				pageSize,
				work.id
			);
			return textNodes;
		},

		translation(parent, { id, slug }, { token }) {
			// patch for repo error
			if (
				parent.full_urn ===
				'urn:cts:greekLit:tlg0525.tlg001.perseus-eng2'
			) {
				return {
					id: -1,
					title: 'Description of Greece Translation',
					urn: 'urn:cts:greekLit:tlg0525.tlg001.perseus-eng2',
				};
			}
			const translationService = new TranslationService({ token });
			return translationService.getTranslation(
				id,
				slug,
				parent.dataValues.id
			);
		},

		version(parent, { id, slug }, { token }) {
			const versionService = new VersionService({ token });
			return versionService.getVersion(id, slug, parent.dataValues.id);
		},

		work_type(parent) {
			if (
				parent.full_urn ===
				'urn:cts:greekLit:tlg0525.tlg001.perseus-eng2'
			) {
				return 'translation';
			}
			return parent.work_type;
		},
	},
};

export default resolvers;
