// I think this file is now obsolete
// * // import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';
// import { ApolloServer } from 'apollo-server';
// import { GraphQLSchema, execute, subscribe } from 'graphql';

// import RootQuery from './graphql/queries/rootQuery';
// import RootMutation from './graphql/mutations/rootMutation';

// /**
//  * Root schema
//  * @type {GraphQLSchema}
//  */
// const RootSchema = new GraphQLSchema({
// 	query: RootQuery,
// 	mutation: RootMutation,
// });

// // Set user and project in context
// const getGraphQLContext = (req) => ({
// 	user: req.user,
// 	project: req.project,
// });

// /**
//  * Set up the graphQL HTTP endpoint
//  * @param	{Object} app 	express app instance
//  */
// export default function setupGraphql(app) {
// 	app.use(
// 		'/graphql',
// 		graphqlExpress((req) => ({
// 			schema: RootSchema,
// 			context: getGraphQLContext(req),
// 		}))
// 	);

// 	app.use(
// 		'/graphiql',
// 		graphiqlExpress({
// 			endpointURL: '/graphql',
// 		})
// 	);
// }
